﻿import re
textlist=[
u"لا تنسى",
u"لا تحشى",
u"لم تحشى",
u"لم يحشى",
u"لم نحشى",
u"لم أحشى",
]


replacements=[
(re.compile(ur"لا ت(..)ى",re.UNICODE),ur"لا ت\1"),
(re.compile(ur"لم ([أنيت])(..)ى",re.UNICODE),ur"لم \1\2"),
]
for text in textlist:
    for replecement in replacements:
        pat= replecement[0];
        rep= replecement[1];
        text2=pat.sub(rep,text);
        if text2!=text:
            print (u"\t".join([text, text2])).encode('utf8')