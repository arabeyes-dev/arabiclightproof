# Options and title texts for the Settings and conditional rules
#
# The Lightproof dialogs contain only grouped checkboxes.
#
# Format of the dialog definition:
#
# GroupID: OptionID, OptionID ...
# Group2ID: OptionID, OptionID ...
# ...
# [Language_code=title of the window]
# OptionID=title of the option
# Option2ID=title of the option
#
# The first language is the default language for other locales
# (use en_US or the common language of your country)
#
# The OptionIDs declared here are used in the rules, too. For example:
#
# foo <- option("style") -> bar # bar is far better
#
# this rule depends from the state of the "style" checkbox.

# options (starred options are default checked ones)

spelling: wordpart, comma, *numpart, grammar
proofreading: style, dup, dup2, dup3, compound, allcompound, *money
typography: ligature, noligature, math

# titles

[en_US=Grammar checking (Hungarian)]

spelling=Spelling
wordpart=Word parts of compounds
comma=Comma usage
proofreading=Proofreading
style=Style checking
compound=Underline typo-like compound words
allcompound=Underline all generated compound words
grammar=Use grammar rules with ambiguous word analysis
money=Consistency of money amount with digits and letters
duplication=Word duplication
dup=Duplication within clauses
dup2=Duplication within sentences
dup3=Allow previous checkings with affixes
numpart=Separation of large numbers
typography=Typography
ligature=Ligature suggestion
noligature=Underline ligatures
math=Typographical signs for fractures, indices etc.

[hu_HU=Nyelvi ellenőrzés (magyar)]

spelling=Helyesírás
wordpart=Külön- és egybeírási javaslatok 
comma=Vesszőhasználatra vonatkozó javaslatok
numpart=Nagy számok ezres tagolása nem törő szóközökkel
proofreading=Korrektúra
style=Stílusellenőrzés
compound=Egyszerű nem szótári összetett szavak aláhúzása
allcompound=Minden nem szótári összetett szó aláhúzása
grammar=Még több javaslat (kevésbé egyértelmű esetekben is)
dup=Szóismétlés keresése tagmondaton belül
dup2=Szóismétlés keresése mondaton belül
dup3=Szóismétlés keresése eltérő toldalékok esetén is
money=Számjegyekkel és betűvel is leírt pénzmennyiségek konzisztenciája
typography=Tipográfia
ligature=F-ligatúrák javaslata
noligature=F-ligatúrák elutasítása
math=Egyéb tipográfiai jelek javaslata (törtek, indexek, szűkebb ezrestagolók)
