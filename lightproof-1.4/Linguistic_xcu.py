# -*- encoding: UTF-8 -*-
"""<?xml version="1.0" encoding="UTF-8"?>
<oor:component-data oor:name="Linguistic"
	oor:package="org.openoffice.Office" xmlns:oor="http://openoffice.org/2001/registry"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<node oor:name="ServiceManager">

		<node oor:name="GrammarCheckers">
			<node oor:name="org.openoffice.comp.pyuno.Lightproof.%s"
				oor:op="fuse">
				<prop oor:name="Locales" oor:type="oor:string-list">
					<value>%s ar-SA ar-DZ ar-BH ar-EG ar-IQ ar-JO ar-KW ar-LB ar-LY ar-MA ar-OM ar-QA ar-SD ar-SY ar-TN ar-AE ar-YE</value>"
				</prop>
			</node>
		</node>
        
    </node>

</oor:component-data>
"""
