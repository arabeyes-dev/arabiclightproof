#generate arabic lightproof

del /Q META-INF\*
del /Q pythonpath\*
del /Q dialog\*
del /Q *xcu
del /Q *xml
# README_ar_DZ.txt
python make.py -v 0.1 -d data ar_DZ
mkdir lightproof-ar_DZ-0.1
mkdir lightproof-ar_DZ-0.1\META-INF
mkdir lightproof-ar_DZ-0.1\pythonpath
mkdir lightproof-ar_DZ-0.1\dialog

copy META-INF  lightproof-ar_DZ-0.1\META-INF
copy pythonpath  lightproof-ar_DZ-0.1\pythonpath
copy dialog  lightproof-ar_DZ-0.1\dialog
copy Lightproof.py   lightproof-ar_DZ-0.1
copy *.xcu  lightproof-ar_DZ-0.1
copy *.xml  lightproof-ar_DZ-0.1
copy README_ar_DZ.txt  lightproof-ar_DZ-0.1
