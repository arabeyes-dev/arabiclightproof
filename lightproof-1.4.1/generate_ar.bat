#generate arabic lightproof

del /Q META-INF\*
del /Q pythonpath\*
del /Q dialog\*
del /Q *xcu
del /Q *xml
# README_ar_DZ.txt
python make.py -v 0.1 -d data ar
mkdir lightproof-ar-0.1
mkdir lightproof-ar-0.1\META-INF
mkdir lightproof-ar-0.1\pythonpath
mkdir lightproof-ar-0.1\dialog

copy META-INF  lightproof-ar-0.1\META-INF
copy pythonpath  lightproof-ar-0.1\pythonpath
copy dialog  lightproof-ar-0.1\dialog
copy Lightproof.py   lightproof-ar-0.1
copy *.xcu  lightproof-ar-0.1
copy *.xml  lightproof-ar-0.1
copy README_ar_DZ.txt  lightproof-ar-0.1
